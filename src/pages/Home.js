import Banner from '../components/Banner';
import Highlight from '../components/Highlight';

export default function Home(){
	return (
		<>
			<Banner />
			<Highlight />
		</>
			)
}
