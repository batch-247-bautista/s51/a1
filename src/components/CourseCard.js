/*  s50 activity own codes:

import React from 'react';
import Button from 'react-bootstrap/Button';
import { Card } from 'react-bootstrap';

export default function CourseCard(course) {
  return (
    <Card>
      <Card.Header as="h5">{course.name}</Card.Header>
      <Card.Body>
        <Card.Text><strong>Sample Course</strong></Card.Text>
        <Card.Text>Description:</Card.Text>
        <Card.Text>This is a sample course offering.</Card.Text>
        <Card.Text>Price:</Card.Text>
        <Card.Text>PHP40,000.00</Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
};
*/

import { Card, Button } from 'react-bootstrap';
import {useState} from 'react';
import PropTypes from 'prop-types';


export default function CourseCard(props){

// checks to see if the data was successfully passed
//console.log(props);
// Every component receives information in a form of an object.
//console.log(typeof props);


// Use the state hook for this component to be able to store its state

// States are used to keep track of information related tp individual components


// syntax
  // const [ getter, setter ] = useState(initialGetterValue);


const [enrollees, setEnrollees] = useState(0);


// using the state hook returns an array with the first element being a value and the second element as a function taht's used to chaneg the value of the first element.

  console.log(useState(0));

  function enroll(){
    if (enrollees < 30) {
      setEnrollees(enrollees + 1);
    } else {
      alert('No more seats available!');
    }
  };

  return (
  <Card>
    <Card.Body>
      <Card.Title>{props.courseProp.name}</Card.Title>
      <Card.Subtitle>Description</Card.Subtitle>
      <Card.Text>{props.courseProp.description}</Card.Text>
      <Card.Subtitle>Price</Card.Subtitle>
      <Card.Text>{props.courseProp.price}</Card.Text>
      <Card.Text>Enrollees: {enrollees}</Card.Text>
      <Button className="bg-primary" onClick={enroll}>Enroll</Button>
    </Card.Body>
  </Card>
  )
}


// Check if the Coursecard component is getting the correct prop types
// PropTypes are used for validation information passed to a component and is a tool normally used to help developers ensure the correct information is apssed from one component to the next.

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}